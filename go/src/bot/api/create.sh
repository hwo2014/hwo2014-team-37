#!/bin/sh

echo "Processing json"

echo "package main" > bStruct.go
for f in b*.json
do
	jflect -s `echo $f | cut -d'.' -f1` < $f >> bStruct.go
done
echo "bStruct.go ready"

echo "package main" > sStruct.go
for f in s*.json
do
	jflect -s `echo $f | cut -d'.' -f1` < $f >> sStruct.go
done
echo "sStruct.go ready"

echo "package main" > tStruct.go
for f in t*.json
do
	jflect -s `echo $f | cut -d'.' -f1` < $f >> tStruct.go
done
echo "tStruct.go ready"

go fmt
echo "done."
