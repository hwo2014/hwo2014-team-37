package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math"
	"math/rand"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

var visual chan interface{}
var DEBUG = true

func init() {
	rand.Seed(time.Now().UnixNano())
}

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

/*
func target_speed(targetSpeed, targetDistance float64) {
	var target float64

	if targetSpeed == 0 {
		targetSpeed = 0.000001
	}
	current := states[len(states)-1]
	difference := targetSpeed - current.speed
	percentage := math.Abs(difference) / targetSpeed
	switch {
	case percentage < 0.2:
		fallthrough
	case percentage < 0.4:
		fallthrough
	case percentage < 0.6:
		fallthrough
	case percentage < 0.8:
	default:
		if difference > 0 {
			target = 1.0
		} else {
			target = 0.0
		}
	}
}
*/

func analyzeMap() {
	lanes := track.NumLanes()
	pieces := len(track.Pieces)

	var distSwitch = make([]float64, lanes)
	for i := 0; i < len(distSwitch); i++ {
		distSwitch[i] = -1.0
	}
	var (
		distCorner = -1.0
		nextCorner *Piece
	)

	for i := 0; i < len(track.Pieces); i++ {
		track.Pieces[i].distanceToSwitch = make([]float64, lanes)
		track.Pieces[i].cornerLength = make([]float64, lanes)
	}

	var index = len(track.Pieces) - 1
	// Käydään läpi takaperin kunnes jokaiselle palalle annettu ko arvot
	for track.Pieces[index].distanceToCorner == 0 || track.Pieces[index].distanceToSwitch[0] == 0 {
		piece := &track.Pieces[index]

		if piece.maxSpeed == nil {
			piece.minSpeed = make([]float64, lanes)
			piece.maxSpeed = make([]float64, lanes)
			for i := 0; i < lanes; i++ {
				piece.maxSpeed[i] = 9999.0
			}
			piece.travelTime = make([]float64, lanes)
		}

		if piece.distanceToCorner == 0 {
			// Piece is a corner, distance is 0.
			if piece.IsCorner() {
				distCorner = 0
				piece.distanceToCorner = -1
				nextCorner = &track.Pieces[index]
				piece.nextCorner = nextCorner

				for i := 0; i < lanes; i++ {
					distanceFromCenter := piece.LaneRadius(i)
					piece.cornerLength[i] = math.Abs((piece.Angle * math.Pi) / 180 * distanceFromCenter)
				}
				// vvvv DEBUG, remove vvvv
				/*
					for i := 0; i < lanes; i++ {
						var radius float64
						if piece.Angle < 0 {
							radius = piece.Radius + track.Lanes[i].DistanceFromCenter
						} else {
							radius = piece.Radius - track.Lanes[i].DistanceFromCenter
						}
						speed := math.Sqrt(FRICTION * radius)
						piece.maxSpeed[i] = 0.1 * speed
					}
				*/
				// ^^^^ DEBUG, remove ^^^^
			} else {
				if distCorner > -1.0 {
					// We've found corner before: distance is cumulative distance + length of this piece
					piece.distanceToCorner = distCorner + piece.Length
					distCorner = piece.distanceToCorner
					piece.nextCorner = nextCorner
				}
			}
		}

		if piece.distanceToSwitch[0] == 0 {
			if piece.Switch {
				for i := 0; i < lanes; i++ {
					distSwitch[i] = 0
					piece.distanceToSwitch[i] = -1.0
				}
			} else {
				if distSwitch[0] > -1.0 {
					if !piece.IsCorner() {
						for i := 0; i < lanes; i++ {
							piece.distanceToSwitch[i] = distSwitch[i] + piece.Length
						}
					} else {
						for i := 0; i < lanes; i++ {
							piece.distanceToSwitch[i] = distSwitch[i] + piece.cornerLength[i]
						}
					}
					for i := 0; i < lanes; i++ {
						distSwitch[i] = piece.distanceToSwitch[i]
					}
				}
			}
		}

		index--
		if index < 0 {
			index = pieces - 1
		}
	}

	shortestLanes = shortestPath()
}

func read_msg(reader *bufio.Reader) (msgtype string, data interface{}, err error) {
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	if DEBUG && !crashed {
		fmt.Println(line)
	}
	var msg interface{}
	split := strings.SplitN(line, "\"", 5)
	msgtype = split[3]
	switch msgtype {
	case "join":
		d := bJOIN{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
		//fmt.Printf("%+v\n", &d)
	case "yourCar":
		d := sYOURCAR{}
		err = json.Unmarshal([]byte(line), &d)
		botName = d.Data.Name
		//fmt.Printf("%+v\n", &d)
	case "gameInit":
		d := sGAMEINIT{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
		currentMap = &d
		track = &d.Data.Race.Track
		//fmt.Printf("%+v\n", &d)
		analyzeMap()
		if visual != nil {
			visual <- &d
		}
	case "gameStart":
		d := sGAMESTART{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
		//fmt.Printf("%+v\n", &d)
	case "turboAvailable":
		d := sTURBOAVAILABLE{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
	case "crash":
		d := sCRASH{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
		//fmt.Printf("%+v\n", &d)
	case "spawn":
		d := sSPAWN{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
	case "gameEnd":
		d := sGAMEEND{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
		//fmt.Printf("%+v\n", &d)
	case "carPositions":
		d := sCARPOSITIONS{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
		if visual != nil {
			visual <- &d
		}
		//fmt.Printf("%+v\n", &d)
	case "tournamentEnd":
		d := sTOURNAMENTEND{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
		//fmt.Printf("%+v\n", &d)
	case "dnf":
		d := sDNF{}
		err = json.Unmarshal([]byte(line), &d)
		data = &d
		//fmt.Printf("%+v\n", &d)
	case "error":
		err = json.Unmarshal([]byte(line), &msg)
		data = &msg
		//log.Printf("%+v\n", &msg)
	default:
		err = json.Unmarshal([]byte(line), &msg)
		data = &msg
		//log.Printf("%+v\n", &msg)
		if err != nil {
			return
		}
	}

	return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	if DEBUG && !crashed {
		log.Println(string(payload))
	}
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
	data := make(map[string]string)
	data["name"] = name
	data["key"] = key
	err = write_msg(writer, "join", data)
	return
}

func sendJoinRace(writer *bufio.Writer, name string, key string, trackName string, carCount float64) (err error) {
	data := make(map[string]interface{})
	botid := make(map[string]string)
	botid["name"] = name
	botid["key"] = key
	data["botId"] = botid
	data["trackName"] = trackName
	data["carCount"] = carCount
	err = write_msg(writer, "joinRace", data)
	return
}

func send_ping(writer *bufio.Writer, tick int) (err error) {
	if tick != -1 {
		if sent[tick] {
			log.Printf("Already replied to tick %d\n", tick)
			return
		}
		sent[tick] = true
	}
	err = write_msg(writer, "ping", make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, tick int, throttle float64) (err error) {
	if tick != -1 {
		if sent[tick] {
			log.Printf("Already replied to tick %d\n", tick)
			return
		}
		sent[tick] = true
	}
	err = write_msg(writer, "throttle", float32(throttle))
	return
}

func send_switch(writer *bufio.Writer, tick int, lane string) (err error) {
	if tick != -1 {
		if sent[tick] {
			log.Printf("Already replied to tick %d\n", tick)
			return
		}
		sent[tick] = true
	}
	err = write_msg(writer, "switchLane", lane)
	return
}

type HistoryState struct {
	piece     int
	lane      int
	location  float64
	speed     float64
	delta     float64
	timestamp time.Time
	tick      int
}

func (h History) Delta(from, to time.Time) float64 {
	var start, stop *HistoryState
	for i := len(states) - 1; i >= 0; i-- {
		if stop == nil && h[i].timestamp.Before(to) {
			stop = h[i]
			continue
		}
		if start == nil && h[i].timestamp.Before(from) {
			start = h[i]
			break
		}
	}
	if start == nil {
		start = h[0]
	}
	if stop == nil {
		stop = h[len(h)-1]
	}

	return stop.speed - start.speed
}

type History []*HistoryState

var botstates = make(map[string]History)

type SpeedPhase struct {
	initial, final float64
	target         float64
	start          time.Time
	duration       time.Duration
	initialdelta   float64 // delta during first 0.1s
	traveled       float64
}

func (p SpeedPhase) String() string {
	return fmt.Sprintf("%.0f -> %.1f: %.1f units in %.2fs (throttle: %.2f)\n", p.initial, p.final, p.traveled, p.duration.Seconds(), p.target)
}

var SpeedTests = make([]SpeedPhase, 6)

var states History
var sent = make(map[int]bool)

var (
	currentMap *sGAMEINIT
	previous   *HistoryState
	botName    string
	track      *TrackInfo

	switchesPassed int
	markedSwitch   bool
	shortestLanes  []int
	passedSwitch   bool = true

	crashed bool

	speedPhase = 5
)

type Crash struct {
	speed    float64
	radius   float64
	friction float64
}

var crashes []Crash

var (
	MAGIC               float64 = 1.0
	FRICTION_DO_NOT_USE         = 0.0 // 0.5 tuntuisi olevan about oikea defaultradoilla, saattaisi olla hyvä lähtökohta.
	FRICTION_DEFAULT            = 0.46
)

/*
	// XXX: Välillä lentää ulos seuraavan mutkan aikana nopeudella joka on liian kova edelliseen mutkaan mutta ihan ok seuraavaan! Aiheuttaa ongelmia.
	// !!!: Mahdollisesti tarkasta crashia lisätessä että oliko nopeus ok edelliseen palaan?
	// Alla olevassa kolmikossa ajaa tyytyväisenä kahden ekan mutkan läpi ylinopeutta mutta crash merkataan kolmannen alkuun.

Corner   (switch: false) a: 45°
  [(0) r: 60u, l: 47u, Speed: 5.44-4.75u/t, time: 8.65t]
  [(1) r: 40u, l: 31u, Speed: 4.45-3.88u/t, time: 7.07t]
Corner   (switch: false) a: 45°
  [(0) r: 60u, l: 47u, Speed: 5.44-4.75u/t, time: 8.65t]
  [(1) r: 40u, l: 31u, Speed: 4.45-3.88u/t, time: 7.07t]
Corner   (switch: false) a: 22°
  [(0) r: 110u, l: 43u, Speed: 7.37-6.44u/t, time: 5.86t]
  [(1) r: 90u, l: 35u, Speed: 6.67-5.82u/t, time: 5.30t]
*/

func updateSpeedAproximationsCrash(friction float64) {
	if friction < FRICTION_DEFAULT {
		DRIVE_SAFE = false
	}

	for i := 0; i < len(track.Pieces); i++ {
		if track.Pieces[i].IsCorner() {
			for lane := 0; lane < track.NumLanes(); lane++ {
				speedlimit := math.Sqrt(friction * track.Pieces[i].LaneRadius(lane))
				if track.Pieces[i].maxSpeed[lane] > speedlimit {
					track.Pieces[i].maxSpeed[lane] = speedlimit
				}
			}
		}
	}
}

func updateSpeedAproximationsSuccess(friction float64) {
	for i := 0; i < len(track.Pieces); i++ {
		if track.Pieces[i].IsCorner() {
			for lane := 0; lane < track.NumLanes(); lane++ {
				speedlimit := math.Sqrt(friction * track.Pieces[i].LaneRadius(lane))
				if track.Pieces[i].minSpeed[lane] < speedlimit {
					FRICTION_DO_NOT_USE = friction
					track.Pieces[i].minSpeed[lane] = speedlimit
				}
			}
		}
	}
}

func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	switch msgtype {
	case "join":
		// state := data.(*bJOIN)
		if DEBUG {
			log.Printf("Joined")
		}
		send_ping(writer, -1)
	case "gameStart":
		// state := data.(*sGAMESTART)
		if DEBUG {
			log.Printf("Game started")
		}
		crashed = false
		send_ping(writer, -1)
	case "turboAvailable":
		msg := data.(*sTURBOAVAILABLE)
		if DEBUG {
			log.Printf("Turbo: %+v\n", msg)
		}
		send_ping(writer, -1)
	case "crash":
		msg := data.(*sCRASH)
		if DEBUG {
			log.Printf("Someone crashed")
		}

		if msg.Data.Name == botName {
			crashed = true
		}
		prev := botstates[msg.Data.Name][len(botstates[msg.Data.Name])-1]
		radius := track.Pieces[prev.piece].LaneRadius(prev.lane)
		crashes = append(crashes, Crash{speed: prev.speed, radius: radius, friction: math.Pow(prev.speed, 2) / radius})
		friction := math.Pow(prev.speed, 2) / radius
		// In some cases bot will crash during a straight, ignore those.
		if track.Pieces[prev.piece].IsCorner() {
			updateSpeedAproximationsCrash(friction)
		}
		if DEBUG {
			log.Println(track)
			log.Printf("%20s: Crashed at lane %d, piece %d (%.0f°), %.2f radius and speed of %.2f\n", msg.Data.Name, prev.lane, prev.piece, track.Pieces[prev.piece].Angle, radius, prev.speed)
			log.Printf("Friction %.2f\n", friction)
		}
		send_ping(writer, -1)
		// XXX: tiputetaan vähän frictionista pois ja kerrotana 0.1 koska se on atm se nopeuskerroin.
		//speedlimit := math.Sqrt((crashes[len(crashes)-1].friction-0.05)*crashes[len(crashes)-1].radius) * 0.1
	case "spawn":
		msg := data.(*sSPAWN)
		if DEBUG {
			log.Printf("%20s: Respawned", msg.Data.Name)
		}
		if msg.Data.Name == botName {
			crashed = false
		}
		send_ping(writer, -1)
	case "gameEnd":
		// state := data.(*sGAMEEND)
		log.Println(track)
		log.Println("Game/round ended(?)")
		sent = make(map[int]bool)
		send_ping(writer, -1)
	case "tournamentEnd":
		log.Println(track)
		log.Println("Tournament ended")
		send_ping(writer, -1)
		os.Exit(0)
	case "carPositions":
		state := data.(*sCARPOSITIONS)
		self := state.Self()

		var current *HistoryState
		for _, botData := range state.Data {
			saveCurrentState(&botData, state)
		}
		current = botstates[self.Id.Name][len(botstates[self.Id.Name])-1]
		if previous == nil {
			previous = current
		} else {
			previous = botstates[self.Id.Name][len(botstates[self.Id.Name])-2]
		}

		if !crashed {
			if speedPhase < 4 {
				DoSpeedTests(writer, state, current, previous)
				return
			}

			if shouldSwitch(writer, self, state) {
				return
			}

			if shouldBrake(writer, current, self, state) {
				return
			}

			if shouldThrottle(writer, current, self, state) {
				return
			}
		}
		send_ping(writer, state.GameTick)

	case "error":
		if DEBUG {
			log.Printf(fmt.Sprintf("Got error: %v", data))
		}
		send_ping(writer, -1)
	default:
		if DEBUG {
			log.Printf("Got msg type: %s", msgtype)
		}
		send_ping(writer, -1)
	}
	return
}

// TODO: Print when passed corner

func shouldBrake(writer *bufio.Writer, current *HistoryState, self *CarPosition, state *sCARPOSITIONS) bool {
	// wantedSpeed < currenSpeed && wantedSpeed <= currentSpeed - (distance/currentSpeed * brakePower)
	maxspeed := self.Piece().nextCorner.maxSpeed[self.Lane()]
	minspeed := self.Piece().nextCorner.minSpeed[self.Lane()]

	if self.Piece().IsCorner() && self.NextPiece().IsCorner() {
		maxspeed = math.Min(maxspeed, self.NextPiece().maxSpeed[self.Lane()])
		minspeed = math.Min(minspeed, self.NextPiece().minSpeed[self.Lane()])
	}

	wantedspeed := (maxspeed + minspeed) / 2
	distance := self.Piece().distanceToCorner - self.PiecePosition.InPieceDistance
	speed := current.speed
	// TODO: dynamic brakepower
	brakepower := 0.001

	if DEBUG {
		if self.Piece().IsCorner() {
			log.Printf("In a corner. Distance: %.0f/%.0f  Speed %.2f/%.2f/%.2f\n", self.PiecePosition.InPieceDistance, self.Piece().cornerLength[self.Lane()], minspeed, speed, maxspeed)
		} else {
			log.Printf("Approaching %.2f-%.2fu/s corner with speed of %.2fu/s. Distance: %.0f\n", minspeed, maxspeed, speed, distance)
		}

		log.Printf("wantedspeed: %.2f, speed: %.2f, distance/speed: %.2f, total: %.4f", wantedspeed, speed, distance/speed, distance/speed*brakepower)
	}

	if wantedspeed < speed && wantedspeed <= speed-((distance/speed)*brakepower) {

		if DEBUG {
			log.Printf("Going to brake from speed %.2f , delta is %.4f", speed, speed-botstates[self.Id.Name][len(botstates[self.Id.Name])-1].speed)
		}

		send_throttle(writer, state.GameTick, 0.0)
		return true
	}
	return false
}

// XXX: 0.1 on hihavakio ja perustuu siihen että nopeus on 10*throttle
var THROTTLE_MULTIPLIER = 0.1
var DRIVE_SAFE = true

func shouldThrottle(writer *bufio.Writer, current *HistoryState, self *CarPosition, state *sCARPOSITIONS) bool {
	var (
		speedlimit float64
		minspeed   float64
	)

	if DEBUG {
		log.Println(crashes)
	}

	if !self.Piece().IsCorner() {
		speedlimit = 10
	} else if DRIVE_SAFE {
		// TODO: if at first place, use minSpeed?
		// Use FRICTION_DEFAULT until minspeed > value_we_got_with_FRICTION_DEFAULT or first crash with friction <= FRICTION_DEFAULT ?
		speedlimit = self.Piece().nextCorner.SpeedWithFriction(FRICTION_DEFAULT, self.Lane())
		minspeed = self.Piece().nextCorner.minSpeed[self.Lane()]
		if minspeed > speedlimit {
			speedlimit = minspeed
		}
	} else {
		speedlimit = (self.Piece().nextCorner.minSpeed[self.Lane()] + self.Piece().nextCorner.maxSpeed[self.Lane()]) / 2
	}

	MAGIC = math.Min(1.0, speedlimit*THROTTLE_MULTIPLIER)

	send_throttle(writer, state.GameTick, MAGIC)
	return true
}

func shouldSwitch(writer *bufio.Writer, self *CarPosition, state *sCARPOSITIONS) bool {

	if self.Piece().Switch {
		passedSwitch = true
	}
	if self.PiecePosition.PieceIndex == switchIndexes()[switchesPassed]-1 && passedSwitch {
		passedSwitch = false
		index := switchesPassed
		if shortestLanes[switchesPassed] == -1 {
			if switchesPassed == len(shortestLanes)-1 {
				index = 0
			} else {
				index++
			}
		}
		lane := ""
		if shortestLanes[index] < self.Lane() {
			lane = "Left"
		} else if shortestLanes[index] > self.Lane() {
			lane = "Right"
		}

		if !markedSwitch {
			if switchesPassed == len(shortestLanes)-1 {
				switchesPassed = 0
			} else {
				switchesPassed++
			}
			markedSwitch = true
		}
		if lane == "" {
			return false
		}
		send_switch(writer, state.GameTick, lane)
		return true
	} else {
		markedSwitch = false
	}
	return false

}

func saveCurrentState(botPosition *CarPosition, state *sCARPOSITIONS) {
	var prev *HistoryState
	if len(botstates[botPosition.Id.Name]) == 0 {
		prev = &HistoryState{
			tick:      state.GameTick,
			piece:     botPosition.PiecePosition.PieceIndex,
			lane:      botPosition.Lane(),
			timestamp: time.Now(),
		}
	} else {
		prev = botstates[botPosition.Id.Name][len(botstates[botPosition.Id.Name])-1]
	}

	location := botPosition.PiecePosition.InPieceDistance
	var speed float64
	if botPosition.PiecePosition.PieceIndex == prev.piece && state.GameTick != prev.tick {
		speed = (location - prev.location) / float64(state.GameTick-prev.tick)
		if DEBUG && !crashed {
			fmt.Printf("%20s: Speed: %.2f | Delta: %.2f (%d)\n", botPosition.Id.Name, speed, speed-prev.speed, speedPhase)
		}
	} else {
		speed = prev.speed
	}

	current := &HistoryState{
		tick:      state.GameTick,
		piece:     botPosition.PiecePosition.PieceIndex,
		location:  location,
		lane:      botPosition.Lane(),
		speed:     speed,
		delta:     speed - prev.speed,
		timestamp: time.Now(),
	}

	if track.Pieces[prev.piece].IsCorner() && !track.Pieces[current.piece].IsCorner() {
		radius := track.Pieces[prev.piece].LaneRadius(prev.lane)
		friction := math.Pow(prev.speed, 2) / radius
		updateSpeedAproximationsSuccess(friction)
	}

	botstates[botPosition.Id.Name] = append(botstates[botPosition.Id.Name], current)
}

func DoSpeedTests(writer *bufio.Writer, state *sCARPOSITIONS, current, previous *HistoryState) {
	var (
		PHASE1 = 0.1
		PHASE2 = 0.0
		PHASE3 = 0.2
		PHASE4 = 0.0
	)

	speedChange := states.Delta(time.Now().Add(-time.Millisecond*100), time.Now())

	switch speedPhase {
	case 0:
		if SpeedTests[speedPhase].traveled == 0 {
			SpeedTests[speedPhase].start = time.Now()
			SpeedTests[speedPhase].initial = current.speed
			SpeedTests[speedPhase].target = PHASE1
		}
		if current.speed > 0.1 && math.Abs(speedChange) < 0.001 {
			send_throttle(writer, state.GameTick, PHASE2)
			SpeedTests[speedPhase].final = current.speed
			SpeedTests[speedPhase].duration = time.Now().Sub(SpeedTests[speedPhase].start)
			SpeedTests[speedPhase].traveled += float64(current.tick-previous.tick) * math.Max(current.speed, previous.speed)
			SpeedTests[speedPhase].initialdelta = states.Delta(SpeedTests[speedPhase].start, SpeedTests[speedPhase].start.Add(time.Millisecond*100))
			speedPhase++
			return
		}
		SpeedTests[speedPhase].traveled += float64(current.tick-previous.tick) * math.Max(current.speed, previous.speed)
		send_throttle(writer, state.GameTick, PHASE1)
		return
	case 1:
		if SpeedTests[speedPhase].traveled == 0 {
			SpeedTests[speedPhase].start = time.Now()
			SpeedTests[speedPhase].initial = current.speed
			SpeedTests[speedPhase].target = PHASE2
		}
		if current.speed > 0.01 {
			SpeedTests[speedPhase].traveled += float64(current.tick-previous.tick) * math.Max(current.speed, previous.speed)
			send_throttle(writer, state.GameTick, PHASE2)
			return
		}
		SpeedTests[speedPhase].final = current.speed
		SpeedTests[speedPhase].duration = time.Now().Sub(SpeedTests[speedPhase].start)
		SpeedTests[speedPhase].traveled += float64(current.tick-previous.tick) * math.Max(current.speed, previous.speed)
		SpeedTests[speedPhase].initialdelta = states.Delta(SpeedTests[speedPhase].start, SpeedTests[speedPhase].start.Add(time.Millisecond*100))
		speedPhase++
		send_throttle(writer, state.GameTick, PHASE3)
		return
	case 2:
		if SpeedTests[speedPhase].traveled == 0 {
			SpeedTests[speedPhase].start = time.Now()
			SpeedTests[speedPhase].initial = current.speed
			SpeedTests[speedPhase].target = PHASE3
		}
		if current.speed > 0.1 && math.Abs(speedChange) < 0.001 {
			SpeedTests[speedPhase].final = current.speed
			SpeedTests[speedPhase].duration = time.Now().Sub(SpeedTests[speedPhase].start)
			SpeedTests[speedPhase].traveled += float64(current.tick-previous.tick) * math.Max(current.speed, previous.speed)
			SpeedTests[speedPhase].initialdelta = states.Delta(SpeedTests[speedPhase].start, SpeedTests[speedPhase].start.Add(time.Millisecond*100))
			speedPhase++
			send_throttle(writer, state.GameTick, PHASE4)
			return
		}
		SpeedTests[speedPhase].traveled += float64(current.tick-previous.tick) * math.Max(current.speed, previous.speed)
		send_throttle(writer, state.GameTick, PHASE3)
		return
	case 3:
		if SpeedTests[speedPhase].traveled == 0 {
			SpeedTests[speedPhase].start = time.Now()
			SpeedTests[speedPhase].initial = current.speed
			SpeedTests[speedPhase].target = PHASE4
		}
		if current.speed > 0.01 {
			SpeedTests[speedPhase].traveled += float64(current.tick-previous.tick) * math.Max(current.speed, previous.speed)
			send_throttle(writer, state.GameTick, PHASE4)
			return
		}
		SpeedTests[speedPhase].final = current.speed
		SpeedTests[speedPhase].duration = time.Now().Sub(SpeedTests[speedPhase].start)
		SpeedTests[speedPhase].traveled += float64(current.tick-previous.tick) * math.Max(current.speed, previous.speed)
		SpeedTests[speedPhase].initialdelta = states.Delta(SpeedTests[speedPhase].start, SpeedTests[speedPhase].start.Add(time.Millisecond*100))
		speedPhase++
		send_ping(writer, state.GameTick)
		if DEBUG {
			log.Println(SpeedTests)
		}
		return
	}
}

func parse_and_dispatch_input(writer *bufio.Writer, msgtype string, input interface{}) (err error) {
	err = dispatch_msg(writer, msgtype, input)
	if err != nil {
		return
	}
	return
}

func bot_loop(conn net.Conn, name string, key string, trackName string, carCount float64) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	if trackName == "" {
		send_join(writer, name, key)
	} else {
		sendJoinRace(writer, name, key, trackName, carCount)
	}
	for {
		msgtype, input, err := read_msg(reader)
		if err != nil {
			log_and_exit(err)
		}
		err = parse_and_dispatch_input(writer, msgtype, input)
		if err != nil {
			log_and_exit(err)
		}
	}
}

func parse_args() (host string, port int, name string, key string, trackName string, carCount float64, err error) {
	args := os.Args[1:]
	if len(args) > 6 {
		return "", 0, "", "", "", 1, errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", "", 1, fmt.Errorf("Could not parse port value to integer: %v\n", args[1])
	}
	name = args[2]
	key = args[3]
	if len(args) == 4 {
		return host, port, name, key, "", 0, nil
	}
	trackName = args[4]
	carCount, err = strconv.ParseFloat(args[5], 64)

	return
}

func log_and_exit(err error) {
	log.Println(track)
	log.Println(crashes)
	if visual != nil {
		close(visual)
	}
	log.Fatal(err)
	os.Exit(1)
}

func main() {
	host, port, name, key, trackName, carCount, err := parse_args()

	if err != nil {
		log_and_exit(err)
	}

	if DEBUG {
		fmt.Println("Connecting with parameters:")
		fmt.Printf("host=%v, port=%v, bot name=%v, key=%v, trackName=%v, carCount=%.0f\n", host, port, name, key, trackName, carCount)
	}

	conn, err := connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	defer conn.Close()

	err = bot_loop(conn, name, key, trackName, carCount)

	if visual != nil {
		close(visual)
	}
}
