package main

type tCREATERACE struct {
	Data struct {
		BotId struct {
			Key  string `json:"key"`
			Name string `json:"name"`
		}
		CarCount  float64 `json:"carCount"`
		Password  string  `json:"password"`
		TrackName string  `json:"trackName"`
	}
	MsgType string `json:"msgType"`
}
type tJOINRACE struct {
	Data struct {
		BotId struct {
			Key  string `json:"key"`
			Name string `json:"name"`
		}
		CarCount  float64 `json:"carCount"`
		Password  string  `json:"password"`
		TrackName string  `json:"trackName"`
	}
	MsgType string `json:"msgType"`
}
