package main

type bJOIN struct {
	Data struct {
		Key  string `json:"key"`
		Name string `json:"name"`
	}
	GameTick int    `json:"gameTick, omitempty"`
	MsgType  string `json:"msgType"`
}
type bPING struct {
	GameTick int    `json:"gameTick, omitempty"`
	MsgType  string `json:"msgType"`
}
type bSWITCHLANE struct {
	Data     string `json:"data"`
	GameTick int    `json:"gameTick, omitempty"`
	MsgType  string `json:"msgType"`
}
type bTHROTTLE struct {
	Data     float64 `json:"data"`
	GameTick int     `json:"gameTick, omitempty"`
	MsgType  string  `json:"msgType"`
}
