package main

import (
	"bytes"
	"fmt"
	"log"
	"math"
)

type sCARPOSITIONS struct {
	Data     []CarPosition `json:"data"`
	GameId   string        `json:"gameId"`
	GameTick int           `json:"gameTick"`
	MsgType  string        `json:"msgType"`
}

func (p sCARPOSITIONS) Self() *CarPosition {
	for i := 0; i < len(p.Data); i++ {
		if p.Data[i].Id.Name == botName {
			return &p.Data[i]
		}
	}
	log.Printf("Couldn't find own car from %+v\n", p)
	return &p.Data[0] // could return nil but since we shouldnt get here anyway lets just prevent random crash.
}

type CarPosition struct {
	Id struct {
		Name  string `json:"name"`
		Color string `json:"color"`
	}
	Angle         float64 `json:"angle"`
	PiecePosition struct {
		PieceIndex      int     `json:"pieceIndex"`
		InPieceDistance float64 `json:"inPieceDistance"`
		Lane            struct {
			StartLaneIndex int `json:"startLaneIndex"`
			EndLaneIndex   int `json:"endLaneIndex"`
		}
		Lap float64 `json:"lap"`
	}
}

func (p CarPosition) DistanceToCorner() float64 {
	return p.Piece().distanceToCorner - p.PiecePosition.InPieceDistance
}

func (p CarPosition) Piece() *Piece {
	return &currentMap.Data.Race.Track.Pieces[p.PiecePosition.PieceIndex]
}

func (p CarPosition) NextPiece() *Piece {
	return &currentMap.Data.Race.Track.Pieces[(p.PiecePosition.PieceIndex+1)%track.NumPieces()]
}

func (p CarPosition) Lane() int {
	// Endlane is usually the one you'll want when calculating things.
	return p.PiecePosition.Lane.EndLaneIndex
}

type sTURBOAVAILABLE struct {
	Data struct {
		TurboDurationMilliseconds float64 `json:"turboDurationMilliseconds"`
		TurboDurationTicks        int     `json:"turboDurationTicks"`
		TurboFactor               float64 `json:"turboFactor"`
	}
	MsgType string `json:"msgType"`
}

type sCRASH struct {
	Data struct {
		Color string `json:"color"`
		Name  string `json:"name"`
	}
	GameId   string `json:"gameId"`
	GameTick int    `json:"gameTick"`
	MsgType  string `json:"msgType"`
}
type sDNF struct {
	Data struct {
		Car struct {
			Color string `json:"color"`
			Name  string `json:"name"`
		}
		Reason string `json:"reason"`
	}
	GameId   string `json:"gameId"`
	GameTick int    `json:"gameTick"`
	MsgType  string `json:"msgType"`
}
type sFINISH struct {
	Data struct {
		Color string `json:"color"`
		Name  string `json:"name"`
	}
	GameId   string `json:"gameId"`
	GameTick int    `json:"gameTick"`
	MsgType  string `json:"msgType"`
}
type sGAMEEND struct {
	Data struct {
		BestLaps []sLAPINFO `json:"bestLaps"`
		Results  []sLAPINFO `json:"results"`
	}
	MsgType string `json:"msgType"`
}

type sLAPINFO struct {
	Car struct {
		Name  string `json:"Name"`
		Color string `json:"Color"`
	}
	Result struct {
		Lap    int     `json:"lap"`
		Laps   int     `json:"laps"`
		Ticks  int     `json:"ticks"`
		Millis float64 `json:"millis"`
	}
}

type sGAMEINIT struct {
	Data struct {
		Race struct {
			Track TrackInfo `json:"track"`

			RaceSession struct {
				Laps         int     `json:"laps"`
				DurationMs   int     `json:"durationMs"`
				MaxLapTimeMs float64 `json:"maxLapTimeMs"`
				QuickRace    bool    `json:"quickRace"`
			}
			Cars []CarInfo `json:"cars"`
		}
	}
	MsgType string `json:"msgType"`
}

type TrackInfo struct {
	StartingPoint struct {
		Position struct {
			X float64 `json:"x"`
			Y float64 `json:"y"`
		}
		Angle float64 `json:"angle"`
	}
	Id     string     `json:"id"`
	Lanes  []LaneInfo `json:"lanes"`
	Name   string     `json:"name"`
	Pieces []Piece    `json:"pieces"`
}

func (t TrackInfo) String() string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "%s - Lanes: %d, Pieces: %d (friction: %f)\n", t.Name, t.NumLanes(), t.NumPieces(), FRICTION_DO_NOT_USE)
	for i, p := range t.Pieces {
		fmt.Fprintf(&b, "[%3d] %s\n", i, p)
	}
	return b.String()
}

func (t TrackInfo) NumLanes() int {
	return len(t.Lanes)
}

func (t TrackInfo) NumPieces() int {
	return len(t.Pieces)
}

type LaneInfo struct {
	DistanceFromCenter float64 `json:"distanceFromCenter"`
	Index              int     `json:"index"`
}

type CarInfo struct {
	Id struct {
		Name  string `json:"name"`
		Color string `json:"color"`
	}
	Dimensions struct {
		Length            float64 `json:"length"`
		Width             float64 `json:"width"`
		GuideFlagPosition float64 `json:"guideFlagPosition"`
	}
}

type Piece struct {
	Length float64 `json:"length"`
	Switch bool    `json:"switch"`
	Radius float64 `json:"radius"`
	Angle  float64 `json:"angle"`

	cornerLength     []float64
	minSpeed         []float64
	maxSpeed         []float64
	travelTime       []float64
	distanceToCorner float64
	distanceToSwitch []float64
	nextCorner       *Piece
}

func (p Piece) String() string {
	var b bytes.Buffer
	if p.IsCorner() {
		fmt.Fprintf(&b, "Corner   (switch: %t) a: %.0f° ", p.Switch, p.Angle)
		for i := 0; i < len(p.cornerLength); i++ {
			// XXX: use p.travelTime when it's supported.
			fmt.Fprintf(&b, "\n  [(%d) r: %.0fu, l: %.0fu, Speed: %.2f-%.2fu/t, time: %.2ft]", i, p.LaneRadius(i), p.cornerLength[i], p.minSpeed[i], p.maxSpeed[i], p.cornerLength[i]/p.minSpeed[i])
		}
	} else {
		fmt.Fprintf(&b, "Straight (switch: %t) l: %.0fu, toCorner: %.0fu, time: %.2ft ", p.Switch, p.Length, p.distanceToCorner, p.travelTime[0])
	}
	return b.String()
}

func (p Piece) SpeedWithFriction(friction float64, lane int) float64 {
	return math.Sqrt(friction * p.LaneRadius(lane))
}

func (p Piece) IsCorner() bool {
	return p.Length == 0
}

func (p Piece) LaneRadius(lane int) float64 {
	if !p.IsCorner() {
		return p.Radius
	}
	if p.Angle > 0 {
		return p.Radius - currentMap.Data.Race.Track.Lanes[lane].DistanceFromCenter
	}
	return p.Radius + currentMap.Data.Race.Track.Lanes[lane].DistanceFromCenter
}

type sGAMESTART struct {
	MsgType string `json:"msgType"`
}
type sLAPFINISHED struct {
	Data struct {
		Car struct {
			Color string `json:"color"`
			Name  string `json:"name"`
		}
		LapTime struct {
			Lap    int     `json:"lap"`
			Millis float64 `json:"millis"`
			Ticks  int     `json:"ticks"`
		}
		RaceTime struct {
			Laps   int     `json:"laps"`
			Millis float64 `json:"millis"`
			Ticks  int     `json:"ticks"`
		}
		Ranking struct {
			FastestLap int `json:"fastestLap"`
			Overall    int `json:"overall"`
		}
	}
	GameId   string `json:"gameId"`
	GameTick int    `json:"gameTick"`
	MsgType  string `json:"msgType"`
}
type sSPAWN struct {
	Data struct {
		Color string `json:"color"`
		Name  string `json:"name"`
	}
	GameId   string `json:"gameId"`
	GameTick int    `json:"gameTick"`
	MsgType  string `json:"msgType"`
}
type sTOURNAMENTEND struct {
	MsgType string `json:"msgType"`
}
type sYOURCAR struct {
	Data struct {
		Color string `json:"color"`
		Name  string `json:"name"`
	}
	MsgType string `json:"msgType"`
}
