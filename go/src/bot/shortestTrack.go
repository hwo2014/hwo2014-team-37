package main

import (
	"math"
)

func lengthOfLanes(piece Piece) []float64 {
	laneLengths := make([]float64, track.NumLanes())
	if piece.IsCorner() {
		for i := 0; i < track.NumLanes(); i++ {
			distanceFromCenter := piece.LaneRadius(i)
			laneLengths[i] = math.Abs((piece.Angle * math.Pi) / 180 * distanceFromCenter)
		}
	}
	return laneLengths
}

func fromSwitchToSwitch(begin int, end int) []float64 {
	laneSpecificLength := make([]float64, track.NumLanes())
	for i := begin + 1; i < end; i++ {
		pieceLaneLegths := lengthOfLanes(track.Pieces[i])
		for j := 0; j < track.NumLanes(); j++ {
			laneSpecificLength[j] += pieceLaneLegths[j]
		}
	}
	return laneSpecificLength
}

func smallestVal(array []float64) int {
	min := 0
	for i := 0; i < len(array); i++ {
		if array[i] < array[min] {
			min = i
		}
	}
	if array[min] == 0 {
		min = -1
	}
	return min
}

func switchIndexes() []int {
	switches := make([]int, 0)
	for i := 0; i < track.NumPieces(); i++ {
		if track.Pieces[i].Switch {
			switches = append(switches, i)
		}
	}
	return switches
}

func shortestPath() []int {
	shortestLanesBetweenSwitches := make([]int, 0)
	switches := switchIndexes()
	for i := 0; i < len(switches); i++ {
		begin := switches[i]
		end := 0
		if i != len(switches)-1 {
			end = switches[i+1]
		} else {
			end = switches[0]
		}

		shortesetLane := smallestVal(fromSwitchToSwitch(begin, end))
		shortestLanesBetweenSwitches = append(shortestLanesBetweenSwitches, shortesetLane)
	}

	return shortestLanesBetweenSwitches

}
